module gitlab.com/guap-programming/2/saod/kp

go 1.13

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/rs/cors v1.6.0
	github.com/vektah/gqlparser/v2 v2.0.1
)
