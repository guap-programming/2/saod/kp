package graph

import (
	"gitlab.com/guap-programming/2/saod/kp/avl"
	"gitlab.com/guap-programming/2/saod/kp/hashtable"
	"gitlab.com/guap-programming/2/saod/kp/list"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	visitors      hashtable.HashTable
	hotelRooms    *avl.Node
	registrations *list.List
}

func NewResolver() *Resolver {
	return &Resolver{
		visitors:      hashtable.CreateHashTable(),
		hotelRooms:    &avl.Node{},
		registrations: list.New(),
	}
}
