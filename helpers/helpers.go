package helpers

import (
	"fmt"
	"gitlab.com/guap-programming/2/saod/kp/errors"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const DateFormat = "02-01-2006"

func PassportNumberToInt(passportNumber string) (int, error) {
	re := regexp.MustCompile("^[0-9]{4}-[0-9]{6}$")
	if re.FindAllString(passportNumber, 1) == nil {
		return 0, errors.WrongPassportNumberError(passportNumber)
	}
	return strconv.Atoi(passportNumber[:4] + passportNumber[5:])
}

func HotelNumberToInt(hotelNumber string) (int, error) {
	re := regexp.MustCompile("^[ЛПОМ][0-9]{3}$")
	if re.FindAllString(hotelNumber, 1) == nil {
		return 0, errors.WrongHotelNumberError(hotelNumber)
	}
	hotelNumber = strings.Trim(hotelNumber, "ЛПОМ")
	return strconv.Atoi(hotelNumber)
}

func StringToTime(str string, format string) (time.Time, error) {
	tm, err := time.Parse(format, str)
	if err != nil {
		return tm, fmt.Errorf("failed to decode time: %v", err)
	}

	return tm, nil
}
