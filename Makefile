.PHONY: precommit gen run

precommit:
	gofmt -w -s -d .
	go vet .
	go mod tidy
	go mod verify
	golangci-lint run
	golint ./...

gen:
	go run github.com/99designs/gqlgen generate

run:
	go run server.go